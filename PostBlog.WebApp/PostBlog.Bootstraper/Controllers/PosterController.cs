﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PostBlog.Bootstraper.Models;
using PostBlog.Common.Enums;
using PostBlog.Common.IdentiyAccess;
using PostBlog.Common.Models;
using PostBlog.Common.Services.Interfaces;

namespace PostBlog.Bootstraper.Controllers
{
    public class PosterController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly ILogger<PosterController> _logger;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public PosterController(
            IPostRepository postRepository,
            ILogger<PosterController> logger,
            UserManager<ApplicationUser> userManager,
            IAuthenticationRepository authenticationService,
            IHttpContextAccessor httpContextAccessor)
        {
            _postRepository = postRepository;
            _userManager = userManager;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }
        // GET: Poster
        public ActionResult Index()
        {

            var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var isEditor = User.IsInRole(RolesEnum.Editor.ToString());

            var listPost = isEditor ? _postRepository.FindAll().OrderByDescending(t=> t.AuditCreatedOnDate).ToList()
                                    : _postRepository.FindByCondition(t => t.UserId == userId).ToList();

            PostIndexViewModel post = new PostIndexViewModel
            {
                Post = new Post(),
                ListPost = listPost
            };

            return View(post);
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: Poster/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostIndexViewModel postView)
        {
            try
            {
                _postRepository.Create(new Post
                {
                    PostId = Guid.NewGuid().ToString(),
                    Title = postView.Post.Title,
                    Content = postView.Post.Content,
                    PostAutor = _userManager.Users.FirstOrDefault().UserName,
                    AuditCreatedOnDate = DateTime.Now,
                    PostState = StateEnum.SUBMITTED.ToString(),
                    PostNumber = Guid.NewGuid().ToString(),
                    UserId = _userManager.Users.FirstOrDefault().Id
                });  
                _postRepository.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex) 
            {
                ViewBag["errores "] = ex.Message;

                return View();
            }
        }

        // GET: Poster/Edit/5
        public ActionResult Edit(string id)
        {
            var post = _postRepository.FindByCondition(p => p.PostId == id).FirstOrDefault();

            var disabled = false;
            if (User.IsInRole(nameof(RolesEnum.Writer)))
            {
                switch (post.PostState)
                {
                    case nameof(StateEnum.PENDING_PUBLISH_APPROVAL):
                        disabled = true;
                        break;
                    case nameof(StateEnum.APPROVED):
                        disabled = true;
                        break;
                    case nameof(StateEnum.REJECTED):
                        disabled = false;
                        break;
                    default:
                        break;
                }
            }
            PostIndexViewModel postViewModel = new PostIndexViewModel
            {
               Post = post,
               DisableField = disabled
            };
            return View(postViewModel);
        }

        // POST: Poster/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PostIndexViewModel postView)
        {
            try
            {

                var post = _postRepository.FindByCondition(p => p.PostId == postView.Post.PostId).FirstOrDefault();
                var state = postView.Post.PostState;
                if (User.IsInRole(nameof(RolesEnum.Writer)))
                {
                    if (post.PostState == nameof(StateEnum.REJECTED))
                    {
                        state = nameof(StateEnum.PENDING_PUBLISH_APPROVAL);
                    }
                }

                var approvedDate = (User.IsInRole(nameof(RolesEnum.Editor)) && state == nameof(StateEnum.APPROVED)) ? DateTime.Now : post.ApprovedDate;
                _postRepository.Update(new Post
                {
                    PostId = post.PostId,
                    Title = postView.Post.Title,
                    Content = postView.Post.Content,
                    PostNumber = post.PostNumber,
                    PostAutor = _userManager.Users.FirstOrDefault().UserName,
                    PostState = state,
                    UserId = post.UserId,
                    AuditCreatedOnDate = post.AuditCreatedOnDate,
                    ApprovedDate  = approvedDate
                });

                _postRepository.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        // GET: Poster/Delete/5
        public ActionResult Delete(string id)
        {
            var post = _postRepository.FindByCondition(p => p.PostId == id).FirstOrDefault();
            _postRepository.Delete(post);
            _postRepository.SaveChanges();
            return RedirectToAction("Index", "Poster");
        }

        // POST: Poster/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}