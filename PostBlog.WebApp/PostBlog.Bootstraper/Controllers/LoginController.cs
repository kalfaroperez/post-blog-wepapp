﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PostBlog.Common.IdentiyAccess;
using PostBlog.Common.Services;
using PostBlog.Common.Services.Interfaces;

namespace PostBlog.Bootstraper.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<LoginController> _logger;
        private readonly IAuthenticationRepository _authenticationService;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public LoginController(
            ILogger<LoginController> logger, 
            IAuthenticationRepository authenticationService,
            SignInManager<ApplicationUser> signInManager)
        {
            _logger = logger;
            _authenticationService = authenticationService;
            _signInManager = signInManager;
        }

        // GET: Login/Create
        public IActionResult SignIn()
        {
            return View();
        }

        // POST: Login/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(LoginModel login)
        {
            try
            {
                // TODO: Add insert logic here
                var userStateLogin = _authenticationService.Sigin(login.Username, login.Password);
                _signInManager.RefreshSignInAsync(userStateLogin.Result.User);
                if (userStateLogin.Result.Status)
                {
                    return RedirectToAction("Index", "Poster");
                }
                return RedirectToAction(nameof(SignIn),"Login");
            }
            catch
            {
                return View();
            }
        }

       
        public IActionResult SingOut()
        {
            try
            {
                _authenticationService.SigOut();
                
                return RedirectToAction("Index", "PublicViewer" );
            }
            catch
            {
                return View();
            }
        }
    }
}