﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PostBlog.Bootstraper.Models;
using PostBlog.Common.Enums;
using PostBlog.Common.Models;
using PostBlog.Common.Services.Interfaces;

namespace PostBlog.Bootstraper.Controllers
{
    public class PublicViewerController : Controller
    {
        private readonly IPostRepository _postRepository;
        private readonly ILogger<PosterController> _logger;
        private readonly IPostDetailRepository _postDetailRepository;
        public PublicViewerController(
            IPostRepository postRepository,
            IPostDetailRepository postDetailRepository,
            ILogger<PosterController> logger)
        {
            _postRepository = postRepository;
            _postDetailRepository = postDetailRepository;
            _logger = logger;
        }

        // GET: PublicViewer
        public ActionResult Index()
        {
            var listPost = _postRepository.FindByCondition(t=>t.PostState == StateEnum.APPROVED.ToString());
            if (listPost.Any())
            {
                return View(listPost);
            }
            List<Post> list = new List<Post>();
            return View(list);
        }

        // GET: PublicViewer/Details/5
        public ActionResult AddComments(string id)
        {
            var post = _postRepository.FindByCondition(t => t.PostId == id).FirstOrDefault();
            var postDetails = _postDetailRepository.FindByCondition(t => t.PostId == post.PostId).OrderByDescending(t=>t.AuditCreatedOnDate).ToList();
            PostViewModel postViewModel = new PostViewModel
            {
                PostId = post.PostId,
                Title = post.Title,
                Content = post.Content,
                PostDetails = postDetails
            };
            return View(postViewModel);
        }

        // POST: PublicViewer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddComments(PostViewModel postView)
        {
            try
            {
                _postDetailRepository.Create(new PostDetail
                {
                    PostDetailtId = Guid.NewGuid().ToString(),
                    PostId = postView.PostId,
                    Comments = postView.PostDetail.Comments,
                    CommentsAutor = postView.PostDetail.CommentsAutor,
                    AuditCreatedOnDate = DateTime.Now
                });

                _postDetailRepository.SaveChanges();
                return Redirect($"AddComments?id={postView.PostId}");
            }
            catch 
            {
                return View();
            }
        }

        // GET: PublicViewer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PublicViewer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PublicViewer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PublicViewer/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}