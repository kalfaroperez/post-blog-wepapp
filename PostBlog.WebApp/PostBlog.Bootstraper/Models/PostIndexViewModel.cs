﻿using PostBlog.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostBlog.Bootstraper.Models
{
    public class PostIndexViewModel
    {
        public Post Post { get; set; }
        public List<Post> ListPost { get; set; }
        public bool DisableField { get; set; }
    }
}
