﻿using PostBlog.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostBlog.Bootstraper.Models
{
    public class PostViewModel
    {
        
        public string Title { get; set; }
        public string Content { get; set; }
        public string Autor { get; set; }
        public string PostId { get; set; }
        public string PostDate { get; set; }
        public PostDetail PostDetail { get; set; }
        public List<PostDetail> PostDetails { get; set; }
    }
}
