﻿using PostBlog.Common.Contracts;
using PostBlog.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostBlog.Common.Services.Interfaces
{
    public interface IPostRepository : IRepositoryBase<Post>
    {
        
    } 
}
