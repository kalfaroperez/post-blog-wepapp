﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostBlog.Common.IdentiyAccess
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string Writer = "Writer";
        public const string Editor = "Editor";
    }
}
