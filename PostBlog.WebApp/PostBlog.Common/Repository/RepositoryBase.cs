﻿using Microsoft.EntityFrameworkCore;
using PostBlog.Common.Context;
using PostBlog.Common.Contracts;
using PostBlog.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace PostBlog.Common.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected PostBlogContext PostBlogContext { get; set; }
        public RepositoryBase(PostBlogContext PostBlogContext)
        {
            this.PostBlogContext = PostBlogContext;
        }
        public IQueryable<T> FindAll()
        {
            return this.PostBlogContext.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.PostBlogContext.Set<T>().Where(expression).AsNoTracking();
        }
        public void Create(T entity)
        {
            this.PostBlogContext.Set<T>().Add(entity);
        }
        public void Update(T entity)
        {
            this.PostBlogContext.Set<T>().Update(entity);
        }
        public void Delete(T entity)
        {
            this.PostBlogContext.Set<T>().Remove(entity);
        }
        public int SaveChanges()
        {
            return this.PostBlogContext.SaveChanges();
        }
    }
}
