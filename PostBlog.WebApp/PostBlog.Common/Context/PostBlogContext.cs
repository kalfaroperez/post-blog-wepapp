﻿
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PostBlog.Common.IdentiyAccess;
using PostBlog.Common.Models;

namespace PostBlog.Common.Context
{
    public partial class PostBlogContext : IdentityDbContext<ApplicationUser>
    {
        public PostBlogContext(DbContextOptions<PostBlogContext> options) : base(options)
        {

        }

        public virtual DbSet<ApplicationUser> ApplicationUser { get; set; }

        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<PostDetail> PostDetails { get; set; }

        //public virtual DbSet<Post> Posts { get; set; }
        //public virtual DbSet<PostDetail> PostDetails { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        
    }
}