﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace PostBlog.Common.Models
{
    public partial class PostDetail
    {
        [Key]
        public string PostDetailtId { get; set; }
        public string Comments { get; set; }
        public DateTime AuditCreatedOnDate { get; set; }
        public string CommentsAutor{ get; set; }
        public string PostId { get; set; }
        
    }
}
