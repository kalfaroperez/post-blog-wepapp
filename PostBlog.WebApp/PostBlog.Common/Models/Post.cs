﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace PostBlog.Common.Models
{
    public partial class Post
    {
        [Key]
        public string PostId { get; set; }
        public string PostNumber { get; set; }
        public string Content { get; set; }
        public DateTime AuditCreatedOnDate { get; set; }
        public string UserId { get; set; }
        public string PostState { get; set; }
        public string PostAutor { get; set; }
        public string Title { get; set; }
        public DateTime ApprovedDate { get; set; }
    }
}
