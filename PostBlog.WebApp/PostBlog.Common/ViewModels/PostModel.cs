﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostBlog.Common.ViewModels
{
    public class PostModel
    {
        public string PostId { get; set; }
        public string Autor { get; set; }
        public DateTime SubmittedDate { get; set; }
        public string State { get; set; }
    }
}
