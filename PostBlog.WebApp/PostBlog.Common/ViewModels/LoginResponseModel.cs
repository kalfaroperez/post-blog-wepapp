﻿using PostBlog.Common.IdentiyAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace PostBlog.Common.ViewModels
{
    public class LoginResponseModel
    {
        public bool Status { get; set; }
        public string Role { get; set; }
        public ApplicationUser User { get; set; }
    }
}
