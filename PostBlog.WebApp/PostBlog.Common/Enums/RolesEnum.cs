﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PostBlog.Common.Enums
{
    public enum RolesEnum
    {
        Writer,
        Editor,
        Admin
    }
}
